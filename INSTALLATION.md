# Notice d'intallation du projet Coinchotte

Afin de rendre l'installation de l'ensemble des briques nécessaire au projet de jeu en ligne Coinchotte la plus simple possible, nous avons décidé d'utiliser docker pour **conteneuriser chaque service de l'application** et docker compose pour **orchestrer et gérer les conteneurs comme un ensemble unifié**.

En effet, Docker est un outil de virtualisation au niveau du système d'exploitation qui permet de développer, déployer et exécuter des applications dans des conteneurs permettant de **ne pas se soucier de la compatibilité et des dépendances entre les environnements de développement et de production**.


## Prérequis:

1. **Docker**
Cette notice n'a pas pour but de vous expliquer comment installer docker sur votre machine mais vous pouvez trouver des instructions précises pour l'installer directement [sur le site de docker](https://docs.docker.com/engine/install/)


2. **Docker-compose**
De même, vous pouvez trouver des instructions pour installer docker-compose [sur le site de docker](https://docs.docker.com/compose/install/). Notez que docker-compose est installé par défaut avec Docker Desktop sur Windows et MacOS.

## Installation
Afin de rendre l'installation la plus simple possible, nous avons créé un fichier `docker-compose.yml` qui contient la configuration de chaque service nécessaire au projet Coinchotte.

Nous avons également mis à dispostion toutes les images docker nécessaires au projet sur Docker hub:

1. **API** : [coinchotte/api](https://hub.docker.com/r/aimerisson/coinchotte_api/) gérant la logique métier du jeu
2. **WEB** : [coinchotte/web](https://hub.docker.com/r/aimerisson/coinchotte_frontend/) gérant l'interface utilisateur

### Cloner le projet
Pour commencer, vous devez cloner ce projet sur votre machine en utilisant la commande suivante:
```bash
git clone https://forge.univ-lyon1.fr/sae-s3-s4/notices.git
```

une fois le projet cloné, vous pouvez vous déplacer dans le répertoire du projet:
```bash
cd notices
```

### Lancer les services
Pour lancer les services, vous devez exécuter la commande suivante:
```bash
docker-compose up
```

Cette commande va télécharger les images docker nécessaires au projet et lancer les services. Vous pouvez accéder à l'interface utilisateur du projet en ouvrant votre navigateur et en tapant l'adresse suivante: [http://web.coinchotte.local](http://web.coinchotte.local).

Au total, 2 services seront accessibles:

- Web coinchotte : http://web.coinchotte.local
- API coinchotte : http://api.coinchotte.local

À noter que le docker-compose monte un volume sur le service API permettant de ne pas perdre les données si le service est relancé.

### Arrêter les services
Une fois que vous avez terminé, vous pouvez arrêter les services en exécutant la commande suivante:
```bash
docker-compose down
```


## Exposer les services à internet.
Pour un accès Internet complet, configurez votre routeur ou pare-feu pour rediriger les requêtes entrantes vers les services énumérés au dessus.

Si vous utilisez un nom de domaine, associez-le à l'adresse IP de votre serveur et configurez les enregistrements DNS appropriés pour pointer vers vos services.

Enfin, utiliser des services comme NGINX ou Traefik en tant que reverse proxy pour correctement sécuriser les connections entre les services et l'extérieur.
